var express = require('express');
var router = express.Router();
const groupsController=require('../controller/groupController')


router.get('/groups',groupsController.getgroups)
router.get('/groups/:groupId/products',groupsController.activeProducts)
router.get('/groups/products/:id',groupsController.productsById)


router.get('/',groupsController.getAll);

module.exports = router;
