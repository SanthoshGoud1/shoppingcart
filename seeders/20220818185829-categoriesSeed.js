'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('Categories', [{
      name: "Categories1",
      GroupId: 1,
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),

    },
  {
    name: "Categories2",
    GroupId: 2,
    isactive: true,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    name: "Categories3",
    GroupId: 2,
    isactive: true,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    name: "Categories4",
    GroupId: 1,
    isactive: true,
    createdAt: new Date(),
    updatedAt: new Date(),

  }
])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
      await queryInterface.bulkDelete('Categories', null, {});

  }
};
