'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('Products', [{
      name: "product1" ,
       description: "products1 Description", 
       image_url: "https://image1", 
       CategoryId: 1,
       isactive: true,
       createdAt:new Date(),
       updatedAt:new Date()

    },
  {
    name: "product2" ,
    description: "products2 Description", 
    image_url: "https://image2", 
    CategoryId: 1,
    isactive: true,
    createdAt:new Date(),
    updatedAt:new Date()
  },
  {
    name: "product3" ,
    description: "products3 Description", 
    image_url: "https://image3", 
    CategoryId: 2,
    isactive: true,
    createdAt:new Date(),
    updatedAt:new Date()
  },
  {
    name: "product4" ,
       description: "products4 Description", 
       image_url: "https://image4", 
       CategoryId: 2,
       isactive: true,
       createdAt:new Date(),
       updatedAt:new Date()

  }
])
  },

  

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Products', null, {});
  }
};
