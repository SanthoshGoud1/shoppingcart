'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     * 
     * 
     * 
    */
    await queryInterface.bulkInsert('Groups', [{
      name: "Group1",
      description: "Description- of Group1",
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),

    },
    {
      name: "Group2",
      description: "Description- of Group2",
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),

    },
    {
      name: "Group3",
      description: "Description- of Group3",
      isactive: true,
      createdAt: new Date(),
      updatedAt: new Date(),

    },
  ]);
  },
  async down(queryInterface, Sequelize) {
      /**
       * Add commands to revert seed here.
       *
       * Example:
       * await queryInterface.bulkDelete('People', null, {});
       */
       await queryInterface.bulkDelete('Groups', null, {});
    }
  };
