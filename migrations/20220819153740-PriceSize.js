'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
     await queryInterface.addColumn(
      'Products',
      'price',
      {
        type: Sequelize.DataTypes.INTEGER,
        allowNull: true,
      },
  ),
  await queryInterface.addColumn(
    'Products',
    'size',
    {
      type: Sequelize.DataTypes.INTEGER,
      allowNull: true,
    },
)


  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn('Products', 'price'),
    await queryInterface.removeColumn('Products', 'size')
  }
};
